package tests;

import org.junit.*;
import junit.framework.TestCase;
import model.data_structures.HashLinearProbing;
import model.data_structures.Queue;
import model.vo.Taxi;


/**
 * Clase usada para verificar que la estructura Stack (pila) est� correctamente implementada.
 */
public class LinearProbingTest extends TestCase
{

	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

	private HashLinearProbing<String, Integer> tabla;
	
	public void setUp()
	{
		tabla = new HashLinearProbing<String, Integer>(5);
	}
	
    // -----------------------------------------------------------------
    // M�todos de prueba
    // -----------------------------------------------------------------

	public void testSizeAndResize() 
	{
		assertEquals(0, tabla.size()); 
		
		for(int i = 0; i < 3; i++)
		{
			Integer a�adir = new Integer(i);
			tabla.put("a", a�adir);
		}
		//debe ser uno ya que intenta a�adir tres veces la misma key (solo entra una vez)
		assertEquals(1, tabla.size());
		
		String inicio = "abc";
		for(int i = 0; i < 3; i++)
		{
			Integer a�adir = new Integer(i);
			tabla.put(""+inicio.charAt(i), a�adir);
		}
		
		Integer a�adir = new Integer(3);
		tabla.put("d", a�adir);
		
		a�adir = new Integer(4);
		tabla.put("e", a�adir);
		
		//Como ya se supero el limite del 80% de la tabla, el limite nuevo se debi� haber duplicado
		assertEquals(10, tabla.limiteActual);
		
		inicio = "fghi";
		for(int i = 4; i < 8; i++)
		{
			a�adir = new Integer(i);
			tabla.put(""+inicio.charAt(i-4), a�adir);
		}
		
		//Como ya se supero/igual� el limite del 80% de la tabla, el limite nuevo se debi� haber duplicado
		assertEquals(20, tabla.limiteActual);
	}
	
	public void testPutAndHash() 
	{
		//error desconocido de cast
		tabla = new HashLinearProbing<String, Integer>(5);
		String StdIn = "abcdefgh";
		System.out.println("algo");
        for (int i = 0; i < StdIn.length(); i++) {
            String key = ""+ StdIn.charAt(i);
            tabla.put(key, new Integer(i));
        }
        
        int pos = tabla.hash("a");
        String temp = tabla.keys[pos];
        assertTrue(3 == tabla.get(temp).intValue());
        StdIn = "abcddddefgh";
        for (int i = 0; i < StdIn.length(); i++) {
            String key = ""+ StdIn.charAt(i);
            tabla.put(key, new Integer(i));
        }
        pos = tabla.hash("d");
        assertEquals(new Integer(6),tabla.values[pos]);
	}
	
	public void testGet()
	{
		tabla = new HashLinearProbing<String, Integer>(5);
		String StdIn = "abcdefgh";
        for (int i = 0; i < StdIn.length(); i++) {
            String key = ""+ StdIn.charAt(i);
            tabla.put(key, i);
        }
        assertEquals(new Integer(3),tabla.get("d"));
        assertEquals(new Integer(StdIn.length()-1),tabla.get("h"));
	}
	
	public void testDelete()
	{
		String StdIn = "abcdefgh";
        for (int i = 0; i < StdIn.length(); i++) 
        {
            String key = ""+ StdIn.charAt(i);
            tabla.put(key, i);
        }
        tabla.delete("d");
        Queue<String> queue = tabla.keys();
        boolean esta = false;
        while(!queue.isEmpty())
        {
        	String s = queue.dequeue();
        	if(s == "d")
        	{
        		esta = true;
        	}
        	System.out.println(s + " " + tabla.get(s)); 
        }
        assertFalse(esta);
  	}
}
