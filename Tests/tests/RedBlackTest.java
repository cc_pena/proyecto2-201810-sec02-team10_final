package tests;

import junit.framework.TestCase;
import model.data_structures.RedBlackTree;
import model.data_structures.Stack;
import model.vo.Taxi;

public class RedBlackTest extends TestCase
{

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	private RedBlackTree<String, Integer> arbol;

	public void setUp()
	{
		arbol = new RedBlackTree<String, Integer>();
	}

	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------

	/**
	 * Agrega 5 elementos al arbol en el siguiente orden
	 * p4, p3, p5, p2, p1
	 * El arbol debe queda
	 *              p4
	 *          p2      p5
	 *       p1   p3
	 */
	public void setup1()
	{
		arbol.put("p4", 1);
		arbol.put("p3", 2);
		arbol.put("p5", 3);
		arbol.put("p2", 4);
		arbol.put("p1", 5);
	}


	/**
	 * Prueba 1: Verificar el m�todo size
	 * <b>M�todos a probar:</b> <br>
	 * size
	 * <b> Casos de prueba: </b><br>
	 * 1) El arbol est� vac�o, no tiene elementos
	 * 2) El arbol tiene 5 elementos
	 */
	public void testSize()
	{
		//Caso 1:
		assertEquals(0, arbol.size());

		//Caso 2:
		//Agregan elementos al arbol
		setup1();
		//Se verifica que el arbol no est� vac�a
		assertEquals(5, arbol.size());
	}

	/**
	 * Prueba 2: Verificar el m�todo isEmpty
	 * <b>M�todos a probar:</b> <br>
	 * isEmpty
	 * <b> Casos de prueba: </b><br>
	 * 1) El arbol est� vac�o
	 * 2) El arbol no est� vac�o
	 */
	public void testIsEmpty()
	{
		//Caso 1:
		assertTrue(arbol.isEmpty());

		//Caso 2:
		//Agrega aal arbol
		arbol.put("Prueba", 1);
		//Se verifica que el arbol no est� vac�a
		assertFalse(arbol.isEmpty());
	}


	/**
	 * Prueba 3: Verificar el m�todo get
	 * <b>M�todos a probar:</b> <br>
	 * get
	 * <b> Casos de prueba: </b><br>
	 * 1) El arbol tiene el elemnto buscado
	 * 2) El arbol no tiene el elemnto buscado
	 */
	public void testGet()
	{
		//Caso 1:
		setup1();

		//Se obtiene el elemento
		assertNotNull("El elemento debio ser encontrado",arbol.get("p1"));
		assertNotNull("El elemento debio ser encontrado",arbol.get("p2"));
		assertNotNull("El elemento debio ser encontrado",arbol.get("p3"));
		assertNotNull("El elemento debio ser encontrado",arbol.get("p4"));
		assertNotNull("El elemento debio ser encontrado",arbol.get("p5"));

		//Caso 2:
		//Se busca el elemento en la pila
		assertNull("El elemento no debio ser encontrado", arbol.get("no esta"));
	}

	/**
	 * Prueba 4: Verificar el m�todo contains
	 * <b>M�todos a probar:</b> <br>
	 * contains
	 * <b> Casos de prueba: </b><br>
	 * 1) El arbol tiene el elemento
	 * 2) El arbol no tiene el elemento
	 */
	public void testContains()
	{
		//Se agregan elementos
		setup1();

		//Caso 1
		assertTrue("El arbol tiene el elemento",arbol.contains("p3"));
		assertTrue("El arbol tiene el elemento",arbol.contains("p2"));
		assertTrue("El arbol tiene el elemento",arbol.contains("p4"));

		//Caso 2
		assertFalse("El arbol no tiene el elemento", arbol.contains("no esta"));
	}

	/**
	 * Prueba 5: Verificar el m�todo put
	 * <b>M�todos a probar:</b> <br>
	 * put
	 * <b> Casos de prueba: </b><br>
	 * 1) Agrega un elemento mayor que la raiz por la derecha
	 * 2) Agrega un elemento menor que la raiz por izquierda
	 */
	public void testPut()
	{
		//Agrega elementos al arbol
		setup1();

		//Caso 1: 
		arbol.put("p6", 6);
		assertNotNull(arbol.get("p6"));

		//Caso 2: 
		arbol.put("p0", 0);
		assertNotNull(arbol.get("p0"));

	}
	
	/**
	 * Prueba 6: Verificar el m�todo deleteMin
	 * <b>M�todos a probar:</b> <br>
	 * deleteMin
	 * <b> Casos de prueba: </b><br>
	 * 1) Elimina el menor 
	 * 2) Agrega un elemento menor que la raiz por izquierda
	 */
	public void testDeleteMin()
	{
		setup1();
		//Elimina el maximo
		arbol.deleteMin();
		assertNull(arbol.get("p1"));
	}

	public void testDeleteMax()
	{
		setup1();
		//Elimina el maximo
		arbol.deleteMax();
		assertNull(arbol.get("p5"));
	}

	public void testDelete()
	{
		setup1();
		//Eliminar el dado por parametro
		arbol.delete("p1");
		assertNull(arbol.get("p1"));
		
	}

	public void testHeight()
	{
		setup1();
		assertEquals(2,arbol.height());

	}




}
