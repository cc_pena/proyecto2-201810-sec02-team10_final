package model.data_structures;

public class HashNode<K,V>
{
	//-----------------------
	//Atributos
	//-----------------------
	protected K key;

	protected V value;

	protected HashNode<K,V> next;

	//Constructor
	public HashNode(K key, V value)
	{
		this.key = key;
		this.value = value;
		next = null;
	}

	//-------------------------
	//M�todos
	//------------------------

	public K getKey() 
	{
		return key;
	}

	public void setKey(K key) 
	{
		this.key = key;
	}

	public V getValue() 
	{
		return value;
	}

	public void setValue(V value) 
	{
		this.value = value;
	}

	public HashNode<K, V> getNext() 
	{
		return next;
	}

	public void setNext(HashNode<K, V> next) 
	{
		this.next = next;
	}
}
