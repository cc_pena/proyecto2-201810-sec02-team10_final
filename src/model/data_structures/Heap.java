package model.data_structures;

public class Heap <T extends Comparable<T>> 
{
	public static void sort(Comparable[] array)
	{
		int temp = array.length;
		for (int k = array.length/2; k >= 1; k--)
			sink(array, k, array.length);
		while (temp > 1) {
			exch(array, 1, temp--);
			sink(array, 1, temp);
		}
	}

	/**
	 * Agrega un nuevo elemento al arreglo manteniendo el orden de prioridad
	 * @param array al que se agrega
	 * @param k 
	 * @param n
	 */
	private static void sink(Comparable[] array, int k, int n) {
		while (2*k <= n) 
		{
			int j = 2*k;
			if (j < n && less(array, j, j+1))
				j++;
			if (!less(array, k, j)) 
				break;
			exch(array, k, j);
			k = j;
		}
	}

	private static boolean less(Comparable[] array, int i, int j) {
		if(array[i-1] == null || array[j-1] == null)
		{
			return false;
		}
		else
		{
			if(array[i-1].compareTo(array[j-1]) < 0)
			{
				return true;
			}
			return false;
		}
	}

	private static void exch(Comparable[] array, int i, int j) 
	{
		Comparable swap = array[i-1];
		array[i-1] = array[j-1];
		array[j-1] = swap;
	}
}
