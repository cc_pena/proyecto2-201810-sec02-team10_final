package model.data_structures;

public class Node<T>
{
	//-----------------------
	//Atributos
	//-----------------------
	/**
	 * This node is the reference of this element
	 */
	protected T element;
	
	/**
	 * Reference of the next node in the estructure 
	 */
	protected Node<T> next;
	
	//Constructor
	public Node(T pelement)
	{
		this.element = pelement;
		next = null;
	}
	
	//-------------------------
	//M�todos
	//------------------------
	/**
	 * Gives the element of which this node is a reference
	 * @return
	 */
	public T getElement()
	{
		return this.element;
	}
	
	/**
	 * Change the element associated to this node
	 * @param pElement
	 */
	public void setElement(T pElement)
	{
		this.element = pElement;
	}
	
	/**
	 * Get the reference to the next element in the list
	 * @return the next element 
	 */
	public Node<T> getNext(){
		return next;
	}
}
