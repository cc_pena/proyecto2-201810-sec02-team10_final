package model.data_structures;


//BST helper node data type
public class NodeRB <Key extends Comparable<Key>, Value>
{
	private Key key;           // key
	private Value val;         // associated data
	private NodeRB<Key, Value> left, right;  // links to left and right subtrees
	private boolean color;     // color of parent link
	private int size;          // subtree count

	public NodeRB(Key key, Value val, boolean color, int size) {
		this.key = key;
		this.val = val;
		this.color = color;
		this.size = size;
	}
	
	public Key getKey()
	{
		return key;
	}
	
	public void setKey(Key pKey)
	{
		key = pKey;
	}
	
	public Value getValue()
	{
		return val;
	}
	
	public void setValue(Value pVal)
	{
		val = pVal;
	}
	
	public NodeRB<Key, Value> getLeft()
	{
		return left;
	}
	
	public void setLeft(NodeRB<Key, Value> pNode)
	{
		left = pNode;
	}
	
	public NodeRB<Key, Value> getRight()
	{
		return right;
	}
	
	public void setRight(NodeRB<Key, Value> pNode)
	{
		right = pNode;
	}
	
	public boolean getColor()
	{
		return color;
	}
	
	public void setColor(boolean pColor)
	{
		color = pColor;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public void setSize(int pSize)
	{
		size = pSize;
	}
}
