package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import javax.sound.midi.Synthesizer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import API.ITaxiTripsManager;
import model.data_structures.HashLinearProbing;
import model.data_structures.MyList;
import model.data_structures.PriorityQueue;
import model.data_structures.Queue;
import model.data_structures.RedBlackTree;
import model.data_structures.SeparateChaining;
import model.vo.RangoDuracion;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	//Atributos
	private RedBlackTree<String, MyList<Taxi>> companias;
	private RedBlackTree<Double, MyList<Servicio>> arbolBinarioB1;
	private SeparateChaining<String, RedBlackTree<Date, MyList<Servicio>>> tablaHashB2;
	private RedBlackTree<Date, MyList<Servicio>> arbolBinarioC3;
	private SeparateChaining<String, MyList<Servicio>> serviciosPorRangoTiempo;
	private SeparateChaining<String, TaxiConPuntos> taxisPuntos;
	private SeparateChaining<Integer, RedBlackTree<String, MyList<Servicio>>> tablaHash2C;
	public static double latitudReferencia;
	public static double longitudReferencia;

	@Override
	public boolean cargarSistema(String direccionJson) {
		//A
		companias = new RedBlackTree<String, MyList<Taxi>>();
		serviciosPorRangoTiempo = new SeparateChaining<String, MyList<Servicio>>(100);
		//B
		arbolBinarioB1 = new RedBlackTree<Double,MyList<Servicio>>();
		tablaHashB2 = new SeparateChaining<String,RedBlackTree<Date,MyList<Servicio>>>(100);
		//C
		taxisPuntos = new SeparateChaining<>(157);

		arbolBinarioC3 = new RedBlackTree<Date,MyList<Servicio>>();

		tablaHash2C = new SeparateChaining<Integer, RedBlackTree<String, MyList<Servicio>>>(1000);

		JsonParser parser = new JsonParser();		

		int cantidadLatitudesValidas=0;
		int cantidadLongitudesValidas=0;
		double sumaLats = 0;
		double sumaLongs = 0;

		File[] array = new File[1];
		if(direccionJson.equals(DIRECCION_LARGE_JSON))
			array = new File(direccionJson).listFiles();
		else
			array[0] = new File(direccionJson);

		try 
		{
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			for(int a = 0; a < array.length; a++)
			{
				JsonArray arr= (JsonArray) parser.parse(new FileReader(array[a]));
				/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
				for (int i = 0; arr != null && i < arr.size(); i++)
				{
					JsonObject obj= (JsonObject) arr.get(i);

					/* Usar el parametro rango para comparar con startimestamp y endtimestamp*/
					String startTime = obj.get("trip_start_timestamp") == null? "NaN": obj.get("trip_start_timestamp").getAsString();
					String endTime = obj.get("trip_end_timestamp") == null? "NaN": obj.get("trip_end_timestamp").getAsString();

					Date start = null;

					try {
						start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(startTime);
					} catch (ParseException e) {
						//Exception si el string no pudo pasarse al formato de la fecha
						e.printStackTrace();
					}

					String taxi_id = obj.get("taxi_id") == null? "NaN": obj.get("taxi_id").getAsString();


					double trip_miles = obj.get("trip_miles") == null? 0: obj.get("trip_miles").getAsDouble();
					double trip_total = obj.get("trip_total") == null? 0: obj.get("trip_total").getAsDouble();

					String company = obj.get("company") == null? "Independent Owner": obj.get("company").getAsString();

					String trip_id = obj.get("trip_id") == null? "NaN": obj.get("trip_id").getAsString();
					double dropoff_census_tract = obj.get("dropoff_census_tract") == null? 0: obj.get("dropoff_census_tract").getAsDouble();
					double dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude") == null? 0: obj.get("dropoff_centroid_latitude").getAsDouble();

					JsonElement drop = obj.get("dropoff_centroid_location");
					String dropoff_type = drop == null? "NaN": drop.getAsJsonObject().get("type").getAsString();
					double droplat = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
					double droplong = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();

					double dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude") == null? 0: obj.get("dropoff_centroid_longitude").getAsDouble();
					int dropoff_community_area = obj.get("dropoff_community_area") == null? 0: obj.get("dropoff_community_area").getAsInt();

					double extras = obj.get("extras") == null? 0: obj.get("extras").getAsDouble();
					double fare = obj.get("fare") == null? 0: obj.get("fare").getAsDouble();
					String payment_type = obj.get("payment_type") == null? "NaN": obj.get("payment_type").getAsString();

					double pickup_census_tract = obj.get("pickup_census_tract") == null? 0: obj.get("pickup_census_tract").getAsDouble();
					double pickup_centroid_latitude = obj.get("pickup_centroid_latitude") == null? 0: obj.get("pickup_centroid_latitude").getAsDouble();

					JsonElement pick = obj.get("pickup_centroid_location");
					String pickup_type = pick == null? "NaN": pick.getAsJsonObject().get("type").getAsString();
					double picklat = pick == null? 0: pick.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
					double picklong = pick == null? 0: pick.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();

					double pickup_centroid_longitude = obj.get("pickup_centroid_longitude") == null? 0: obj.get("pickup_centroid_longitude").getAsDouble();
					int pickup_community_area = obj.get("pickup_community_area") == null? 0: obj.get("pickup_community_area").getAsInt();

					double tips = obj.get("tips") == null? 0: obj.get("tips").getAsDouble();
					double tolls = obj.get("tolls") == null? 0: obj.get("tolls").getAsDouble();

					String trip_end_timestamp = obj.get("trip_end_timestamp") == null? "NaN": obj.get("trip_end_timestamp").getAsString();
					int trip_seconds = obj.get("trip_seconds") == null? 0: obj.get("trip_seconds").getAsInt();
					String trip_start_timestamp = obj.get("trip_start_timestamp") == null? "NaN": obj.get("trip_start_timestamp").getAsString();

					Servicio serv = new Servicio(trip_id, taxi_id, trip_seconds, trip_miles,trip_total, startTime, pickup_community_area, dropoff_community_area,
							pickup_centroid_latitude, pickup_centroid_longitude);

					if(companias.contains(company))
					{
						MyList<Taxi> taxisDC = companias.get(company);

					}
					Taxi taxi = new Taxi(taxi_id);




					//Parte metodo A1
					SeparateChaining<Integer, MyList<Servicio>> services = taxi.getServicios();
					if(!services.contains(pickup_community_area)) 
						taxi.getServicios().put(pickup_community_area, new MyList<Servicio>() );
					services.get(pickup_community_area).add(serv);

					if(companias.get(company)==null)
					{
						MyList<Taxi> taxis = new MyList<Taxi>();
						companias.put(company, taxis);
					}

					MyList<Taxi> misTaxis = companias.get(company);
					misTaxis.add(taxi);


					//Parte metodo A2
					int  grupo = (int) Math.ceil(trip_seconds/60.0);
					int limSup = grupo*60;
					int limInf = limSup-59;
					if(!serviciosPorRangoTiempo.contains(limInf + "-" +limSup))
					{
						MyList<Servicio> servs = new MyList<Servicio>();
						servs.add(serv);
						serviciosPorRangoTiempo.put(limInf + "-" +limSup, servs);

					}
					else
					{
						MyList<Servicio> servs = serviciosPorRangoTiempo.get(limInf + "-" +limSup);
						servs.add(serv);
					}


					//Parte metodo B1
					if(trip_miles > 0.0)	
					{
						if(!arbolBinarioB1.contains(new Double(trip_miles)))
						{
							MyList<Servicio> lista = new MyList<Servicio>();
							lista.add(serv);
							arbolBinarioB1.put(new Double(trip_miles), lista);
						}
						else
						{
							MyList<Servicio> lista = arbolBinarioB1.get(new Double(trip_miles));
							lista.add(serv);
						}
					}

					//Parte metodo B2
					if(!tablaHashB2.contains(pickup_community_area + "-" +dropoff_community_area))
					{
						RedBlackTree<Date,MyList<Servicio>> arbol = new RedBlackTree<Date,MyList<Servicio>>();
						MyList<Servicio> lista = new MyList<Servicio>();
						lista.addInOrder(serv);
						arbol.put(serv.getStartDate(), lista);
						tablaHashB2.put(pickup_community_area + "-" +dropoff_community_area, arbol);

					}
					else
					{
						RedBlackTree<Date,MyList<Servicio>> arbol = tablaHashB2.get(pickup_community_area + "-" +dropoff_community_area);
						MyList<Servicio> lista = new MyList<Servicio>();
						if(arbol.contains(serv.getStartDate()))
						{
							lista = arbol.get(serv.getStartDate());
							lista.addInOrder(serv);
						}
						else
						{
							lista.addInOrder(serv);
							arbol.put(serv.getStartDate(), lista);
						}
					}

					//Parte Metodo C1
					if(trip_miles*trip_total != 0.0)
					{
						TaxiConPuntos txPuntos = taxisPuntos.get(taxi_id);
						if(txPuntos==null)
						{
							txPuntos = new TaxiConPuntos(taxi_id);
						}
						int cantServs = txPuntos.getCantServicios();
						double recorrida = txPuntos.getTotalMillas();
						double ganancias = txPuntos.getTotalGanancias();
						txPuntos.setcantServicios(cantServs+1);
						txPuntos.setTotalMillas(recorrida+trip_miles);
						txPuntos.setTotalGanancias(ganancias+trip_total);

						taxisPuntos.put(taxi_id, txPuntos);
					}

					//Parte M�todo C2
					if(pickup_centroid_latitude != 0)
					{
						sumaLats += pickup_centroid_latitude;
						cantidadLatitudesValidas++;
					}
					if(pickup_centroid_longitude != 0)
					{
						sumaLongs += pickup_centroid_longitude;
						cantidadLongitudesValidas++;
					}


					//Parte Metodo C3
					if(!arbolBinarioC3.contains(serv.getStartDate()))
					{
						MyList<Servicio> lista = new MyList<Servicio>();
						lista.add(serv);
						arbolBinarioC3.put(serv.getStartDate(), lista);
					}
					else
					{
						MyList<Servicio> lista = arbolBinarioC3.get(serv.getStartDate());
						lista.add(serv);
					}
				}
			}
		}

		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}

		latitudReferencia = sumaLats / cantidadLatitudesValidas;
		longitudReferencia = sumaLongs / cantidadLongitudesValidas;

		return true;
	}

	/**
	 * Arbol con compa�ias (Compa�ia, {Taxis}), las compa�ias estan en orden alfabetico (SE CREO EN CARGAR)
	 * Si taxi.get("company")==null taxi.company= "Independent Owner". (ESTA EN EL CARGAR)
	 * Cada taxi tiene una tabla de hash con sus servicios organizados por area (Area, {Servicios}), 
	 * servicios de cada taxi deben estar ordenados cronol�gicamente por su fecha/hora de inicio. 
	 */
	@Override
	public MyList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String pCompania) {
		MyList<TaxiConServicios> respuesta = new MyList<TaxiConServicios>();
		//Obtener la compa�ia segun el identificador dado por parametro
		MyList<Taxi> taxis = companias.get(pCompania);
		taxis.listing();
		//Recorrer los taxis de la compania
		int maxServ = 0;
		while(true)
		{
			try
			{
				Taxi taxiActual = taxis.getCurrent();
				MyList<Servicio> servDeArea =taxiActual.darServiciosPorArea(zonaInicio);
				int cuantosServ = taxiActual.cantidadServiciosPorArea(zonaInicio);
				if(cuantosServ < maxServ)
				{
					taxis.avanzar();
					//Hace que siga al siguiente sin hacer las siguientes sentencias dentro del while
					continue;
				}
				else
				{
					//Si hay un nuevo maximo se crea una nueva lista que remplaza la ya existente
					if(cuantosServ > maxServ)
					{
						respuesta = new MyList<TaxiConServicios>();
						maxServ = cuantosServ;
					}
					//Si el maximo se repite se agrega a la lista ya existente
					TaxiConServicios anadir = new TaxiConServicios(taxiActual.getTaxiId(), pCompania);
					anadir.agregarServicios(servDeArea);
					respuesta.add(anadir);

				}
				taxis.avanzar();
			}
			catch (Exception e) {
				break;
			}

		}
		return respuesta;
	}

	@Override
	public MyList<Servicio> A2ServiciosPorDuracion(int duracion) {
		int buscar = (int) Math.ceil(duracion/60.0);
		int limSup = buscar*60;
		int limInf = limSup-59;
		MyList<Servicio> respuesta = serviciosPorRangoTiempo.get(limInf + "-" +limSup);	
		return respuesta;
	}

	@Override
	public Queue<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		Queue<Double> cola = arbolBinarioB1.keys(new Double(distanciaMinima), new Double(distanciaMaxima));
		Queue<Servicio> aRetornar = new Queue<Servicio>();
		while(!cola.isEmpty())
		{
			MyList<Servicio> temp = arbolBinarioB1.get(cola.dequeue());
			for(int i = 0; i < temp.size(); i++)
			{
				aRetornar.enqueue(temp.get(i));
			}
		}
		return aRetornar;
	}

	@Override
	public Queue<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		Date fechaInicial = null;
		Date fechaFinal = null;
		try 
		{
			fechaInicial = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(fechaI + "T" + horaI);
		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try 
		{
			fechaFinal = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(fechaF + "T" + horaF);
		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RedBlackTree<Date, MyList<Servicio>> arbol = tablaHashB2.get(zonaInicio + "-" + zonaFinal);
		if(arbol == null)
		{
			return null;
		}
		Queue<Date> cola = arbol.keys(fechaInicial, fechaFinal);
		Queue<Servicio> aRetornar = new Queue<Servicio>();
		while(!cola.isEmpty())
		{
			MyList<Servicio> temp = arbol.get(cola.dequeue());
			for(int i = 0; i < temp.size(); i++)
			{
				aRetornar.enqueue(temp.get(i));
			}
		}
		return aRetornar;
	}

	@Override
	public PriorityQueue<TaxiConPuntos> R1C_OrdenarTaxisPorPuntos() {
		System.out.println("Taxis Ordenados Por Puntos");
		PriorityQueue<TaxiConPuntos> respuesta = new PriorityQueue<>(taxisPuntos.size);
		Queue<String> kys = taxisPuntos.keys();
		System.out.println(kys.dequeue());
		while(!kys.isEmpty())
		{
			String keyActual = kys.dequeue();
			TaxiConPuntos agregar = taxisPuntos.get(keyActual);
			respuesta.insert(agregar);
		}
		return respuesta;
	}

	@Override
	public MyList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		// TODO Auto-generated method stub
		Queue<Double> cola = arbolBinarioB1.keys();
		while(!cola.isEmpty())
		{
			MyList<Servicio> supp = arbolBinarioB1.get(cola.dequeue());
			supp.listing();
			Servicio serv = supp.getCurrent();
			while(true)
			{
				double distancia = getDistance(latitudReferencia, longitudReferencia, serv.getPickupLatitud(), serv.getPickupLongitud()) / 1609.344;

				if(distancia <= 500)
				{
					serv.setDistanceFromReference(distancia);
					String dist = "" + distancia;
					Integer key = Integer.parseInt(dist.split("\\.")[0] + dist.split("\\.")[1].charAt(0) );
					key++;

					System.out.println("la key es " + key + " y la distancia es " + distancia + ". El id es " + serv.getTaxiId());

					if(!tablaHash2C.contains(key))
					{
						RedBlackTree<String,MyList<Servicio>> arbol = new RedBlackTree<String, MyList<Servicio>>();
						MyList<Servicio> lista = new MyList<Servicio>();
						lista.add(serv);
						arbol.put(serv.getTaxiId(), lista);
						tablaHash2C.put(key, arbol);
					}
					else
					{
						RedBlackTree<String,MyList<Servicio>> arbol = tablaHash2C.get(key);
						if(!arbol.contains(serv.getTaxiId()))
						{
							MyList<Servicio> lista = new MyList<Servicio>();
							lista.add(serv);
							arbol.put(serv.getTaxiId(), lista);
						}
						else
						{
							MyList<Servicio> lista = arbol.get(serv.getTaxiId());
							lista.add(serv);
						}
					}
				}
				supp.avanzar();

				try	{supp.getCurrent();} catch (Exception e){break;}

				serv = supp.getCurrent();
			}
		}
		
		String dist = "" + millasReq2C;
		Integer key = Integer.parseInt(dist.split("\\.")[0] + dist.split("\\.")[1].charAt(0) );
		key++;
		
		RedBlackTree<String,MyList<Servicio>> arbol = tablaHash2C.get(key);
		if(arbol != null)
		{
			return arbol.get(taxiIDReq2C);	
		}
		return null;
	}

	@Override
	public MyList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		Date temporal = null;
		try 
		{
			int horas = Integer.parseInt(hora.substring(0,2));
			int minutos = Integer.parseInt(hora.substring(3,5));
			if(minutos < 8)
			{
				hora = hora.replace(hora.substring(3,5), "00");
			}
			else if(minutos < 23)
			{
				hora = hora.replace(hora.substring(3,5), "15");

			}
			else if(minutos < 38)
			{
				hora = hora.replace(hora.substring(3,5), "30");
			}
			else if(minutos < 53)
			{
				hora = hora.replace(hora.substring(3,5), "45");
			}
			else
			{
				hora = hora.replace(hora.substring(3,5), "00");
				horas++;
				if(horas == 24)
				{
					horas = 0;
					Date dt = new SimpleDateFormat("yyyy-MM-dd" ).parse(fecha);
					Calendar c = Calendar.getInstance();
					c.setTime(dt);
					c.add(Calendar.DATE, 1);
					dt = c.getTime();
					SimpleDateFormat ss = new SimpleDateFormat("yyyy-MM-dd" );
					fecha = ss.format(dt);
				}
				if(horas < 10)
				{
					hora = hora.replace(hora.substring(0, 2), "0"+horas);
				}
				else
				{
					hora = hora.replace(hora.substring(0, 2), ""+horas);
				}
			}
			temporal = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(fecha + "T" + hora);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arbolBinarioC3.get(temporal);
	}


	public static double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = Math.toRadians(lat2-lat1);
		Double lonDistance = Math.toRadians(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
		* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

}
