package model.vo;

/**
 * VO utilizado en Req 5A, tiene el rango de distancia y la lista de servicios cuya distancia recorrida 
 * pertenece a dicho rango
 */
public class RangoDuracion  {
	

		//ATRIBUTOS

		/**
		 * Modela el valor minimo del rango
		 */
		private double limiteSuperior;

		/**
		 * Modela el valor maximo del rango
		 */
		private double limiteInferior;
		
		public RangoDuracion(double pLimSuperior, double pLimInfer)
		{
			limiteInferior = pLimInfer;
			limiteSuperior = pLimSuperior;
		}

		//METODOS

		/**
		 * @return the limiteSuperior
		 */
		public double getLimiteSuperior()
		{
			return limiteSuperior;
		}

		/**
		 * @param limiteSuperior the limiteSuperior to set
		 */
		public void setLimiteSuperior(int limiteSuperior) 
		{
			this.limiteSuperior = limiteSuperior;
		}

		/**
		 * @return the limineInferior
		 */
		public double getlimiteInferior() 
		{
			return limiteInferior;
		}

		/**
		 * @param limineInferior the limineInferior to set
		 */
		public void setLimiteInferior(int limineInferior) 
		{
			this.limiteInferior = limineInferior;
		}

		public int hashCode()
		{
			return (int) Math.ceil(limiteSuperior/60.0);	
		}

	}



