package model.vo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.print.attribute.standard.PDLOverrideSupported;



public class Servicio implements Comparable<Servicio>{

	//Atributos

	private String tripId;
	private String taxiId;
	private int tripSeconds;
	private double tripMiles;
	private double tripTotal;
	private String startTime;
	private Date startDate;
	private int pickupZone;
	private int dropOffZone;
	private double pickupLatitud;
	private double pickupLongitud;
	private double distanceFromReference;

	public Servicio(String pTripID, String pTaxiID, int pTripSeconds, double pTripMiles, double pTripTotal, String pStartTime, int pPickupZone, 
			int pDropOffZone, double pPickupLatitud, double pPickupLongitud)
	{
		tripId = pTripID;
		taxiId = pTaxiID;
		tripSeconds = pTripSeconds;
		tripMiles = pTripMiles;
		tripTotal = pTripTotal;
		startTime = pStartTime;
		try 
		{
			startDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(this.startTime);
		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pickupZone = pPickupZone;
		dropOffZone = pDropOffZone;
		pickupLatitud = pPickupLatitud;
		pickupLongitud = pPickupLongitud;
	}


	public String getTripId() {
		return tripId;
	}

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return tripSeconds;
	}
	
	public double getDistanceFromReference()
	{
		return distanceFromReference;
	}
	
	public void setDistanceFromReference(double pDistance)
	{
		distanceFromReference = pDistance;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return tripTotal;
	}

	public Date getStartDate(){
		return startDate;
	}
	
	public String getStartTime(){
		return startTime;
	}

	public int getPickupZone(){
		return pickupZone;
	}

	public int getDropOffZone(){
		return dropOffZone;
	}


	public double getPickupLatitud(){
		return pickupLatitud;
	}

	public double getPickupLongitud(){
		return pickupLongitud;
	}

	/**
	 * Retorna -1 si no se puede pasar la fecha del formato
	 */
	public int hashCode()
	{
		try 
		{
			Date inicio = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(this.startTime);
			return (int) (inicio.getTime()/1000);

		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}

	}

	@Override
	public int compareTo(Servicio arg0) {
		//Comparar por starTime para que queden ordenados cronologicamente
		return this.getTripId().compareTo( arg0.getTripId() );
	}
}