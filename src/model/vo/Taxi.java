package model.vo;

import model.data_structures.MyList;
import model.data_structures.SeparateChaining;

public class Taxi implements Comparable<Taxi> {

	//Atributos

	private String taxiId;

	/**
	 * Hash table de  los Servicios prestados por el taxi
	 * key: pickupArea
	 * value: MyList de servicios */
	private SeparateChaining<Integer, MyList<Servicio>> servicios;

	//Constructor
	public Taxi(String pTaxiId)
	{
		taxiId = pTaxiId;
		servicios = new SeparateChaining<>(157);
	}

	//Metodos
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return taxiId;
	}

	public SeparateChaining<Integer, MyList<Servicio>> getServicios() {
		return servicios;
	}

	public void setServicios(SeparateChaining<Integer, MyList<Servicio>> pServicios) {
		servicios = pServicios;
	}

	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}

	/**
	 * Retorna los servicios que iniciaron en un area dada
	 * @param pArea Area de la que iniciaron los servicios buscados
	 * @return si no hay servicios en el area null, de lo contrario una lista encadenada con los servicios 
	 * 			iniciados en el area param
	 */
	public MyList<Servicio> darServiciosPorArea(int pArea)
	{	
		if(servicios.contains(pArea))
		{
			MyList<Servicio> respuesta = servicios.get(pArea);
			return respuesta;
		}
		else
			return null;
	}
	
	public int cantidadServiciosPorArea(int pArea)
	{
		if(darServiciosPorArea(pArea)!= null)
			return darServiciosPorArea(pArea).size();
		else
			return 0;
	}

	@Override
	public int compareTo(Taxi o)
	{
		// TODO Auto-generated method stub
		return this.getTaxiId().compareTo(o.getTaxiId());
	}
}

