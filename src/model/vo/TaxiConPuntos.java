package model.vo;

public class TaxiConPuntos implements Comparable<TaxiConPuntos> {

	private double puntos;
	
	private double totalMillas;
	
	private double totalGanancias;
	
	private int cantServicios;
	
	private String taxiId;
	
	
	public TaxiConPuntos(String pTaxiId) {
		taxiId = pTaxiId;	
		totalMillas = 0.0;
		totalGanancias = 0.0;
		cantServicios = 0;
		
		puntos = getPuntos();
	}
	
	public String getTaxiId()
	{
		return taxiId;
	}
	
	public void setTotalMillas(double pMillas)
	{
		totalMillas = pMillas;
	}
	
	public void setTotalGanancias(double pGanancias)
	{
		totalGanancias = pGanancias;
	}
	
	public void setcantServicios(int pCuantosServicios)
	{
		cantServicios = pCuantosServicios;
	}
	
	public double getTotalMillas()
	{
		return totalMillas;
	}
	
	public double getTotalGanancias()
	{
		return totalGanancias;
	}
	
	public int getCantServicios()
	{
		return cantServicios;
	}

	/**
	 * se suma el total de millas recorridas y el total de dinero recibido por dicho taxi 
	 * en todos sus servicios y se divide el total de dinero recibido entre el total de millas 
	 * recorridas, multiplicado por el total de servicios prestados 
     * @return puntos - puntos de un Taxi
     */
	public double getPuntos(){
		return (totalGanancias/totalMillas)*cantServicios;
	}
	
	public int compareTo(TaxiConPuntos pTaxi){
		return (int) (this.getPuntos()-pTaxi.getPuntos());
	}
}
