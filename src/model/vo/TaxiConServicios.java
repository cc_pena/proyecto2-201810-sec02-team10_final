package model.vo;

import model.data_structures.MyList;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private MyList<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        this.servicios = new MyList<Servicio>();
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public MyList<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.size();
    }

    public void agregarServicio(Servicio servicio){
        servicios.add(servicio);
    }
    
    public void agregarServicios(MyList<Servicio> pServicios)
    {
    	servicios = pServicios;
    }

    @Override
    public int compareTo(TaxiConServicios o) {
        return taxiId.compareTo( o.getTaxiId());
    }

    public void print(){
        System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
        for(int i=0; i<numeroServicios(); i++)
        {
        	
            System.out.println("\t"+servicios.get(i).getStartTime());
        }
        System.out.println("___________________________________");;
    }
}
